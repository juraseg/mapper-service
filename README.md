# Mini service demonstrating `mapper` library

## LWN parser

Parses https://lwn.net/headlines/rss

After installing run script:

```bash
$ import_lwn
```