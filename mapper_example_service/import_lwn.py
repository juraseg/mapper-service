# -*- coding: utf-8 -*-
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, UnicodeText, Integer, DateTime

import requests

from mapper import schema
from mapper.xmlimport import XMLImporter

Base = declarative_base()

url = 'https://lwn.net/headlines/rss'


class ArticleModel(Base):
    __tablename__ = 'lwn_rss_feed'
    id = Column(Integer, primary_key=True)
    title = Column(UnicodeText)
    url = Column(UnicodeText)
    datetime = Column(DateTime)
    creator = Column(UnicodeText)
    description = Column(UnicodeText)


class ArticleImportSchema(schema.Schema):
    __db_model__ = ArticleModel

    title = schema.Field('title', schema.Str())
    link = schema.Field('url', schema.Str())
    dc_date = schema.Field('datetime', schema.Datetime("%Y-%m-%dT%H:%M:%S+00:00"),
                           override_field_name='dc:date')
    dc_creator = schema.Field('creator', schema.Str(),
                              override_field_name='dc:creator')
    description = schema.Field('description', schema.Str())


def main():
    db_path = os.path.join(os.getcwd(), 'my.db')
    db_uri = 'sqlite:///' + db_path
    engine = create_engine(db_uri)
    session_factory = scoped_session(sessionmaker(bind=engine))

    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    session = session_factory()

    importer = XMLImporter(session, ArticleImportSchema, 'item')

    r = requests.get(url)

    importer.import_records(r.content)


if __name__ == '__main__':
    main()
