# -*- coding: utf-8 -*-
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

setup(name='mapper-example-service',
      version='0.0',
      description='mapper-example-service',
      long_description=README,
      classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Topic :: Database",
        ],
      author='Yuri Abzyanov',
      keywords='rss db',
      packages=find_packages(),
      include_package_data=True,
      license='MIT',
      zip_safe=False,
      install_requires=[
          'sqlalchemy',
          'requests',
          'mapper',
      ],
      dependency_links=[
          'git+https://bitbucket.org/juraseg/mapper.git#egg=mapper',
      ],
      entry_points="""\
      [paste.app_factory]
      main = compmon:main
      [console_scripts]
      import_lwn = mapper_example_service.import_lwn:main
      """,
      )
